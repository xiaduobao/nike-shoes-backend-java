import controller.reponseObj.ShoeInfo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import service.ShoePriceService;

import java.math.BigDecimal;

@SpringBootTest(classes = NikeBackendApp.class)
@RunWith(SpringRunner.class)
public class ShoePriceServiceTest {

    @Autowired
    private ShoePriceService shoePriceService;


    @Test
    public void testGetRandomPrice() {
        ShoeInfo shoeInfo = shoePriceService.getRandomPrice("1");
        assert shoeInfo.getShoeId() == "1";
        assert shoeInfo.getDiscountedPrice() == null;
        assert shoeInfo.getShoePrice().intValue() > 0;
        assert shoeInfo.getMessage() != null;
    }


    @Test
    public void testGetRandomPriceWithWrongId() {
        try {
            ShoeInfo shoeInfo = shoePriceService.getRandomPrice("-1");

        }catch (Exception e){
            assert e instanceof  RuntimeException;
            assert e.getMessage().equals("wrong shoe id");
        }

    }

    @Test
    public void testGetDiscountPrice() {
        ShoeInfo shoeInfo = shoePriceService.getDiscountPrice("1");
        assert shoeInfo.getShoeId() == "1";
        assert shoeInfo.getDiscountedPrice().intValue() > 0;
        assert shoeInfo.getDiscountedPrice().divide(shoeInfo.getShoePrice()).equals(BigDecimal.valueOf(0.4));
        assert shoeInfo.getMessage() != null;
    }

    @Test
    public void testDiscountPriceWithWrongId() {
        try {
            ShoeInfo shoeInfo = shoePriceService.getDiscountPrice("-1");

        }catch (Exception e){
            assert e instanceof  RuntimeException;
            assert e.getMessage().equals("wrong shoe id");
        }

    }
    @Test
    public void testDiscountPriceWithEmptyId() {
        try {
            ShoeInfo shoeInfo = shoePriceService.getDiscountPrice("-");

        }catch (Exception e){
            assert e instanceof  RuntimeException;
            assert e.getMessage().equals("wrong shoe id");
        }

    }
}
