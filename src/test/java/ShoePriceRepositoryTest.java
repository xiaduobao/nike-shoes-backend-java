import org.junit.Test;
import repository.ShoePriceRepository;
import repository.domain.ShoePriceRange;

import java.math.BigDecimal;

public class ShoePriceRepositoryTest {

    @Test
    public void testQueryPriceRangeByNormalId() {
        ShoePriceRepository priceRepository = new ShoePriceRepository();
        ShoePriceRange range = priceRepository.selectShoePriceRange("1");
        assert range.getMinPrice().equals(BigDecimal.valueOf(120));
        assert range.getMaxPrice().equals(BigDecimal.valueOf(150));
    }

    @Test
    public void testQueryPriceRangeByWrongId() {
        ShoePriceRepository priceRepository = new ShoePriceRepository();
        try {
            ShoePriceRange range = priceRepository.selectShoePriceRange("-1");
        } catch (Exception e) {
            assert e instanceof RuntimeException;
            assert e.getMessage().equals("wrong shoe id");
        }

    }
}
