import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = NikeBackendApp.class)
public class ShoePriceControllerTest {
    @Autowired
    private WebApplicationContext context;



    private MockMvc mockMvc;
    @Before
    public void before(){
        this.mockMvc= MockMvcBuilders.webAppContextSetup(this.context).build();
    }
    @Test
    public void testGetRandomPrice() throws Exception{
        // Usually will compare the result content, due to it have random price,
        //so prefer to test the random logic in Service level.
        // for this case, the mockMvc verify the connections and the http status.
        mockMvc.perform(get("/api/fetch-random-price/1"))
                .andExpect(status().isOk());
    }

    @Test
    public void testGetRandomPriceWithWrongUrl() throws Exception{
        mockMvc.perform(get("/api/wrong_url/1"))
                .andExpect(status().is4xxClientError());
    }

}