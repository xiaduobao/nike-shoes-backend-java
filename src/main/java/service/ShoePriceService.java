package service;

import controller.reponseObj.ShoeInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PathVariable;
import repository.ShoePriceRepository;
import repository.domain.ShoePriceRange;

import java.math.BigDecimal;
import java.util.Random;

@Service
public class ShoePriceService {

    @Autowired
    private ShoePriceRepository shoePriceRepository;
    private static final Random RANDOM = new Random();


    private static final BigDecimal DISCOUNT = BigDecimal.valueOf(0.4);

    public ShoeInfo getRandomPrice(String shoeId) {

        Integer randomPrice = this.getRandomPrice();
        ShoeInfo info = new ShoeInfo();
        info.setShoePrice(BigDecimal.valueOf(randomPrice));
        info.setShoeId(shoeId);
        ShoePriceRange shoePriceRange = shoePriceRepository.selectShoePriceRange(shoeId);
        info.setMessage(this.getShoeMessage(info.getShoePrice(), shoePriceRange));
        return info;
    }

    private String getShoeMessage(BigDecimal shoePrice, ShoePriceRange shoePriceRange) {
        Assert.notNull(shoePrice, "shoePrice can't be null");
        if (shoePrice.compareTo(shoePriceRange.getMinPrice()) < 0) {
            return "Best time to buy!";
        } else if (shoePrice.compareTo(shoePriceRange.getMaxPrice()) > 0) {
            return "Can wait for discount.";
        } else {
            return "Moderate state, can buy now!";
        }
    }


    public ShoeInfo getDiscountPrice(String shoeId) {

        Integer randomPrice = this.getRandomPrice();
        ShoeInfo info = new ShoeInfo();
        info.setShoePrice(BigDecimal.valueOf(randomPrice));
        info.setDiscountedPrice(BigDecimal.valueOf(randomPrice).multiply(DISCOUNT));
        info.setShoeId(shoeId);
        ShoePriceRange shoePriceRange = shoePriceRepository.selectShoePriceRange(shoeId);
        info.setMessage(this.getShoeMessage(info.getDiscountedPrice(), shoePriceRange));
        return info;
    }

    private Integer getRandomPrice() {
        return RANDOM.nextInt(200);
    }
}
