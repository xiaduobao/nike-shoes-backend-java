import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@EnableAutoConfiguration
@ComponentScan(basePackages = {"service","repository","controller"})
public class NikeBackendApp {

    @GetMapping("/api/shoe-price/{id}")
    @ResponseBody
    public String getShoePrice(@PathVariable String id) {
        String url = "http://localhost:8081/api/fetch-random-price/1";
        RestTemplate restTemplate = new RestTemplate();
        String result = restTemplate.getForObject(url, String.class);
        return result;
    }


    @GetMapping("/a")
    @ResponseBody
    public String pingping() {

        return "hello World";
    }






    public static void main(String[] args) {
        SpringApplication.run(NikeBackendApp.class, args);
    }

}
