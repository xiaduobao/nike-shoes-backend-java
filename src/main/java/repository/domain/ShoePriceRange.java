package repository.domain;

import java.math.BigDecimal;

public class ShoePriceRange {

    private BigDecimal minPrice;

    private BigDecimal maxPrice;

    private String shoeId;

    public ShoePriceRange(BigDecimal minPrice, BigDecimal maxPrice, String shoeId) {
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
        this.shoeId = shoeId;
    }

    public BigDecimal getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(BigDecimal minPrice) {
        this.minPrice = minPrice;
    }

    public BigDecimal getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(BigDecimal maxPrice) {
        this.maxPrice = maxPrice;
    }

    public String getShoeId() {
        return shoeId;
    }

    public void setShoeId(String shoeId) {
        this.shoeId = shoeId;
    }
}
