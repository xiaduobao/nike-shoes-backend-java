package repository;

import org.springframework.stereotype.Repository;
import repository.domain.ShoePriceRange;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

@Repository
public class ShoePriceRepository {

    private static final
    Map<String, ShoePriceRange> shoePriceRangeSet = new HashMap<>();

    static {
        /**
         * think as database
         */
        shoePriceRangeSet.put("1", new ShoePriceRange(BigDecimal.valueOf(120), BigDecimal.valueOf(150), "1"));
        shoePriceRangeSet.put("2", new ShoePriceRange(BigDecimal.valueOf(5), BigDecimal.valueOf(150), "2"));
        shoePriceRangeSet.put("3", new ShoePriceRange(BigDecimal.valueOf(120), BigDecimal.valueOf(160), "3"));
        shoePriceRangeSet.put("4", new ShoePriceRange(BigDecimal.valueOf(100), BigDecimal.valueOf(130), "4"));
        shoePriceRangeSet.put("5", new ShoePriceRange(BigDecimal.valueOf(180), BigDecimal.valueOf(200), "5"));
        shoePriceRangeSet.put("6", new ShoePriceRange(BigDecimal.valueOf(120), BigDecimal.valueOf(150), "6"));
    }

    public ShoePriceRange selectShoePriceRange(String id) {
        if (shoePriceRangeSet.containsKey(id)) {
            return shoePriceRangeSet.get(id);
        }
        throw new RuntimeException("wrong shoe id");

    }
}
