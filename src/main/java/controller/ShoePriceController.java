package controller;

import controller.reponseObj.ShoeInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;
import service.ShoePriceService;

@RestController
@RequestMapping("/api")
public class ShoePriceController {
    @Autowired
    private ShoePriceService shoePriceService;

    @GetMapping("/fetch-random-price/{id}")
    @ResponseBody
    public ShoeInfo getRandomPrice(@PathVariable String id) {
        Assert.notNull(id, "shoe id can't be null");
        return shoePriceService.getRandomPrice(id);
    }


    @GetMapping("/fetch-discount-price/{id}")
    @ResponseBody
    public ShoeInfo getDiscountPrice(@PathVariable String id) {

        Assert.notNull(id, "shoe id can't be null");
        return shoePriceService.getDiscountPrice(id);
    }

}
