package controller.reponseObj;

import repository.domain.ShoePriceRange;

import java.math.BigDecimal;

public class ShoeInfo {

    private String shoeId;
    private BigDecimal shoePrice;

    private BigDecimal discountedPrice;


    /**
     *  less than its minimum price then 1
     *  between its minimum & maximum price range then  2
     *  current shoe price is greater than its maximum price then 3
     */
    private Integer priceStatus;

    /**
     * message can be hardcode at front end,  however for multi-language website,
     * the message passed by backend could be a better practice.
     * so priceStatus and message just need to have one field.
     */
    private String  message;



    public Integer getPriceStatus() {
        return priceStatus;
    }

    public void setPriceStatus(Integer priceStatus) {
        this.priceStatus = priceStatus;
    }

    public String getShoeId() {
        return shoeId;
    }

    public void setShoeId(String shoeId) {
        this.shoeId = shoeId;
    }

    public BigDecimal getShoePrice() {
        return shoePrice;
    }

    public void setShoePrice(BigDecimal shoePrice) {
        this.shoePrice = shoePrice;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public BigDecimal getDiscountedPrice() {
        return discountedPrice;
    }

    public void setDiscountedPrice(BigDecimal discountedPrice) {
        this.discountedPrice = discountedPrice;
    }

}
