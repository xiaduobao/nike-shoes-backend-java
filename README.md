### Nike Interview Backend Code Challenge [Java]

_The provided code document should contain more details._

This project uses Maven & Spring Boot (Web Starter Pack). So you will need to install `java (> 1.8)` & `maven` on your machine first.
For more information, you can visit the [Spring Boot docs](https://docs.spring.io/spring-boot/docs/current/reference/html/getting-started.html#getting-started.introducing-spring-boot)

1. Clone [this project](https://gitlab.com/hiring_nike_china/fetch-shoe-prices/) and start this nodejs server, following the instructions on this project. The project should run at port 9090
2. You can run `mvn dependency:tree` to list/install the maven dependencies used in this project.
3. To run the server (on port 8081), use this command: `mvn spring-boot:run`

APIs:

1. Get original price (randomly fetched) for a supplied shoe id:
```
URL (GET) - http://localhost:8081/api/shoe-price/1

Response:
{
    "shoePrice": 147
}
```
### Changes & Code Improvements.
1. The code structure added are Controller, Service and Repository, the call chain is Controller->Service->Repository
2. Controller exposes the mapping url for the http request, Adding the Controller intended to have more extensibility. 
3. Service is designed to handle the main logic, some logic could be reused in APIs.
4. Repository is for data access, usually it would query the database, for this case, we write static data instead of querying from DB.
5. Change the getShoePrice url's port from 9090 to 8081.
6. add @ComponentScan Config in NikeBackendApp for Managing the SpringBeans.


_ Unit Test
1. Controller, Service and Repository all have it's own unittest, each test has the normal and exception case.
2. The Controller tests  the connections and  http status by MockMVC.
3. The Service should cover most of the cases as it has the core logic. 
4. Actually, when this project get heavy, we need to take long time to startup the test, so recommend the Mockito to mock our dependency,  it doesn't need to initialize all the SpringBeans.
5. TDD can improve our efficiency in some case.

_ Maven Dependency
1. add the Spring unittest dependency for the project

- API
```
URL (GET) - http://localhost:8081/api/fetch-random-price/1

Response:
{
    "shoeId": "1",
    "shoePrice": 13,
    "message": "Best time to buy!"
}

```
```
URL (GET) - http://localhost:8081/api/fetch-discount-price/1

Response:
{
    "shoeId": "1",
    "shoePrice": 197,
    "discountedPrice": 78.8,
    "priceStatus": null,
    "message": "Best time to buy!"
}

```

### If you more time, what would you add further.

1. firstly, will consider the getState logic move to the backend as it has some data security risk, then, it will just need a state to Front, no need to expose the price range data.
2. secondly, I would reconstruct the code modules to  make it more clean, follow the rule of Low cohesion and high coupling, not let all files in a folder.

### What were your doubts, and what were your assumptions for the projects?

1. not sure whether need to add the front page for the new API of discount shoe price.

### Any other notes, that are relevant to your task.
# _TIPs
1. enable debug=true could help us locate the problem quickly.

# _Security Concerns
1. The shoes price range data don't have to respond to the Front end, which has the security risk if we call the API with different ShoeIds.

# Problems:
1.  Chinese user can't  register the gitlab account due to the network.
2. SpringBoot 3.0.0 requires JDK17, However it will take long time to download.